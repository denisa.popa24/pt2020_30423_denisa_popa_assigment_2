package model;

import Controller.ConcreteStrategyTime;
import Controller.SimulationManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Queue;

public class Server implements Runnable {
    public Thread qThread;
    private Queue<Client> clientsQueue = new LinkedList<>();
    private int queueIndex;

    public int getQueueIndex() {
        return queueIndex;
    }

    private boolean isRunning = false;
    private int totalClientsEver;
    private int totalWaitingTime;
    private ConcreteStrategyTime time;
    private File fileout;

    public Server(int index, ConcreteStrategyTime timer, File fileOut) throws FileNotFoundException {
        this.queueIndex = index;
        this.isRunning = false;
        this.totalClientsEver = 0;
        this.totalWaitingTime = 0;
        this.time = timer;
        this.qThread = new Thread(this);
        PrintStream o = new PrintStream(fileOut);
        System.setOut(o);
    }

    public void addClientToQueue(Client client) {
        totalClientsEver++;
        this.clientsQueue.add(client);
        System.out.println( "Queue " + getQueueIndex() + ": (" + client.getId() + "," + client.getTimeArrival() + "," + client.getTimeService() + ")");
    }

    public void removeClientFromQueue(Client client) {
       System.out.println("Client"+client.getId() +" left queue " + this.queueIndex+" one moment ago") ;
        this.clientsQueue.remove(client);
    }

    public void run() {
        try {
            int timer = 0;
            synchronized (this) {
                while (this.isRunning) {
                    wait();
                    if (this.clientsQueue.size() == 0) {
                        System.out.println("queue" + getQueueIndex() + ": is empty");
                    } else {
                        if (clientsQueue.isEmpty()==false) {
                            Client currentClient = this.clientsQueue.peek();
                            int tService = currentClient.getTimeService();
                            totalWaitingTime+=time.getTime()-currentClient.getTimeArrival();//for average waiting time
                            if (tService == timer) {
                                removeClientFromQueue(currentClient);
                                timer=0;
                            } else {
                                timer++;
                            }
                        }
                    }

                        Thread.sleep(500);
                    }
                }
        } catch (InterruptedException e) {
        }
    }

    public Queue<Client> getClientsQueue() {
        return this.clientsQueue;
    }

    public void start() {
        this.isRunning = true;
        qThread.start();
    }

    public synchronized void awake() {
        notify();
    }

    public void stop() {
        this.isRunning = false;
    }

    public int totalWaitingTime() {
        return totalWaitingTime;
    }
}