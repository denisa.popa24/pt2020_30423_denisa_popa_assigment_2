package model;

import java.util.Random;

public class Client {

    private Integer id;
    private Integer timeArrival;
    private Integer timeService;

    public Client(Integer id, Integer tArrival, Integer tService) {
        this.id = id;
        this.timeArrival = tArrival;
        this.timeService = tService;
    }

    public Integer getId() {
        return id;
    }

    public Integer getTimeArrival() {
        return timeArrival;
    }

    public Integer getTimeService() {
        return timeService;
    }

    public static int generateArrivalTime(int arriveMin, int arriveMax)
    {
            Random r = new Random();
            return r.nextInt((arriveMax -3*arriveMin) + 1) +arriveMin;
        
    }

    public static int generateServiceTime(int serviceMin, int serviceMax)
    {
            Random r = new Random();
            return r.nextInt((serviceMax -serviceMin) + 1) +serviceMin;
    }

    @Override
    public String toString()
    {
        String formatClient = String.format("(%d, %d, %d)", this.id, this.timeArrival, this.timeService);
        return formatClient;
    }

}
