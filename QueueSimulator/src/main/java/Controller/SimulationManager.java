package Controller;

import model.Client;
import model.IdGenerator;
import model.Server;

import java.io.*;
import java.util.ArrayList;

public class SimulationManager implements Runnable {

    private File finput;
    private File foutput;
    private Integer nrClients;
    private Integer nrQueues;
    private Integer tMaxSimulation;
    private Integer tMaxArrival;
    private Integer tMinArrival;
    private Integer tMaxService;
    private Integer tMinService;
    private ArrayList<Client> clients = new ArrayList<Client>();
    private ArrayList<Server> queueServices = new ArrayList<Server>();
    private Thread simCtrlThread;
    private boolean isRunning = true;
    private ConcreteStrategyTime t;
    private int currentSimulationTime = 0;

    public SimulationManager(String filePathIn, String filePathOut) throws FileNotFoundException {
        this.finput = new File(filePathIn);
        this.foutput = new File(filePathOut);
        this.simCtrlThread = new Thread(this);
        this.t = new ConcreteStrategyTime();
        t.startTime();
        PrintStream o = new PrintStream(foutput);
        System.setOut(o);
    }

    public void readFromInputFile() throws NumberFormatException, IOException {
        BufferedReader inputBuff = new BufferedReader(new FileReader(this.finput));
        int[] ar = new int[7];
        int i = 0;
        String string;
        while ((string = inputBuff.readLine()) != null) {
            String[] str = string.split(",");
            if (str.length == 2) {
                ar[i] = Integer.parseInt(str[0]);
                i++;
                ar[i] = Integer.parseInt(str[1]);
                i++;
            } else {
                ar[i] = Integer.parseInt(str[0]);
                i++;
            }
        }
        if (ar.length == 7) {
            this.nrClients = ar[0];
            this.nrQueues = ar[1];
            this.tMaxSimulation = ar[2];
            this.tMinArrival = ar[3];
            this.tMaxArrival = ar[4];
            this.tMinService = ar[5];
            this.tMaxService = ar[6];
        } else {
            System.out.println("Incorrect file");
            return;
        }
    }

    public Integer getNrClients() {
        return nrClients;
    }

    public Integer getNrQueues() {
        return nrQueues;
    }

    public Integer gettMaxSimulation() {
        return tMaxSimulation;
    }

    public Integer gettMaxArrival() {
        return tMaxArrival;
    }

    public Integer gettMinArrival() {
        return tMinArrival;
    }

    public Integer gettMaxService() {
        return tMaxService;
    }

    public Integer gettMinService() {
        return tMinService;
    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    public ArrayList<Client> aux = new ArrayList<>();

    public void clientsGenerator() {

        for (int i = 0; i < this.getNrClients(); i++) {
            int tArrival = Client.generateArrivalTime(this.gettMinArrival(), this.gettMaxArrival());
            int tService = Client.generateServiceTime(this.gettMinService(), this.gettMaxService());
            Client client = new Client(IdGenerator.generateId(), tArrival, tService);
            this.clients.add(client);
        }
        aux = clients;
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                while(isRunning && currentSimulationTime!=gettMaxSimulation()) {
                    wait();
                    this.currentSimulationTime = t.getTime();
                    System.out.println("\nTime:" + currentSimulationTime);
                    System.out.println(("Waiting Clients:"));
                    for (Client client : this.aux) {
                        if(currentSimulationTime< client.getTimeArrival())
                            System.out.println(client.toString());
                    }
                    int i = this.clients.size() - 1;
                    while (this.clients.size() > 0) {
                        Server selectedQ = this.shortestQueue();
                        if (currentSimulationTime >= this.clients.get(i).getTimeArrival()) {
                            selectedQ.addClientToQueue(this.clients.get(i));//add in queue
                            this.clients.remove(i);//remove from list of clients
                        }
                        if (i > 0) {
                            i--;
                        } else {
                            break;
                        }
                    }
                    for (Server q : this.queueServices) {
                        q.awake();
                    }
                    if (this.clients.isEmpty()) {
                        if (this.emptyAllQueues()) {
                            for (Server q : this.queueServices) {
                                q.stop();
                                try {
                                    q.qThread.join();
                                } catch (InterruptedException ie) {
                                    System.out.println("Thread interrupted");
                                }
                             //   q.averageWaitingTime();
                            }
                            this.stop();
                            this.isRunning = false;
                            averageWaitingTime();
                        }
                    }
                    t.time++;
                }
            }
        } catch (InterruptedException e) {
            System.out.println("The main thread has been interrupted");
        }
    }

    public Server shortestQueue() {
        int min = this.nrClients;
        Server currentQueue = null;
        for (Server queue : this.queueServices) {
            if (queue.getClientsQueue().size() <= min) {
                min = queue.getClientsQueue().size();
                currentQueue = queue;
            }
        }
        return currentQueue;
    }

    public synchronized void awake() {
        notify();
    }

    public void manageQueues() throws InterruptedException, FileNotFoundException {
        for (int i = 0; i < this.getNrQueues(); i++) {
            Server s = new Server(i, t, foutput);
            this.queueServices.add(s);
        }
        for (Server q : this.queueServices) {
            q.start();
        }
        simCtrlThread.start();
        while (this.isRunning) {
            Thread.sleep(10);
            this.awake();
        }
    }


    public synchronized void stop() {
        isRunning = false;
        notify();
    }

    private boolean emptyAllQueues() {
        for (Server queue : queueServices) {
            if (!queue.getClientsQueue().isEmpty()) {
                return false;
            }
        }
        return true;
    }
    public void averageWaitingTime(){
        int i=0;
        int totalWTime=0;
        for(Server q: queueServices){
            totalWTime+=this.queueServices.get(i).totalWaitingTime();
            i++;
        }
        System.out.println("average waiting time="+ totalWTime/nrClients);
    }

}
