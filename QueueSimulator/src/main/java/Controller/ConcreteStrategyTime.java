package Controller;

public class ConcreteStrategyTime implements Runnable{

    public int time;
    public Thread timeThread;
    public boolean isRunning = false;

    public ConcreteStrategyTime() {
        this.timeThread = new Thread( this);
        this.time = 0;
    }

    public void startTime() {
        this.isRunning = true;
        timeThread.start();
    }

    public void stopTime() {
        this.isRunning = false;
    }

    @Override
    public void run() {
        try {
            int currentTime = this.getTime();
            Thread.sleep(500);
        } catch (Exception e) {
            System.out.println("The main thread has been interrupted");
        }
        time++;
    }

    public synchronized int getTime() {
        return this.time;
    }


}


