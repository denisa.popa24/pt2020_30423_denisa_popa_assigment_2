import Controller.SimulationManager;
import model.Client;
import java.io.FileWriter;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {

        SimulationManager manager = new SimulationManager("D:\\Assigment2PT2020\\pt2020_30423_denisa_popa_assigment_2\\QueueSimulator\\src\\main\\java\\InputTests\\in-test-2.txt", "D:\\Assigment2PT2020\\pt2020_30423_denisa_popa_assigment_2\\QueueSimulator\\src\\main\\java\\InputTests\\out-test-2.txt");
        manager.readFromInputFile();
        manager.clientsGenerator();
        ArrayList<Client> clients = manager.getClients();
        manager.manageQueues();

         System.out.println(manager.getClients().toString());
        manager.averageWaitingTime();

    }
}
